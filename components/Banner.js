import Jumbotron from 'react-bootstrap/Jumbotron'
import Button from 'react-bootstrap/Button'
export default function Banner(){
	return(
	<Jumbotron>
	    <h1>Welcome to Zuitt Course Booking App!</h1>
	    <p>
	      Opportunities for everyone, everywhere
	    </p>
	    <p>
	      <Button variant="primary">Learn more</Button>
	    </p>
     </Jumbotron>


		)
}

//Mini activity
/*
	1. Transfer the Jumbotron component from the index.js to subcomponent Banner.js
	2. Transfer the Card components from the index.js to subcomponent Highlights.js
	3. Render Banner.js and Highlights to our index.js
	9:17pm
*/ 