import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Banner from '../components/Banner'
import Highlights from '../components/Highlights'

export default function Home() {
  return (
    <div>
      <Head>
        <title>Batch 87 | Course Booking App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Banner/>
      <Highlights/>
    </div>
  )
}


//Head component of nextjs is the head element of the html file