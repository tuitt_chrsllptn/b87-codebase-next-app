import {useState, useEffect, useContext} from 'react'
import Head from 'next/head'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import AppHelper from '../apphelper'
import UserContext from '../UserContext'

export default function login(){

	const {user, setUser} = useContext(UserContext)

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [disable, setDisable] = useState(true) 

	useEffect(()=> {
		if(email !== "" && password !== ""){
			setDisable(false) //enabling the login button
		} else {
			setDisable(true) //disable the login button
		}
	},[email, password])


	const userLogin = (e) => {
		e.preventDefault()
		const payload = {
			method: "POST",
			headers: {
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		}

		fetch(`${AppHelper.API_URL}/users/login`, payload)
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			if(data !== false){
				localStorage.setItem('token', data.access)

				//gets the user details
				const payload = {
					method: "GET",
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${data.access}`
					}
				}

				fetch(`${AppHelper.API_URL}/users/details`, payload)
				.then(res => res.json())
				.then(data => {
					setUser({
						email: data.email,
						isAdmin: data.isAdmin,
						id: data._id
					})
				})
				.catch(error => console.log(error))


			} else {
				alert("Wrong Credentials! Try Again!")
			}
		})
		.catch(error => {
			console.log(error)
		})
	}

	return(
		<div>
			<Head>
				<title> Course Booking | Login</title>
			</Head>

			<Row className="mt-4 pt-4 justify-content-center">
				<Col md={8}>
					<Form onSubmit={e => userLogin(e)}>
					  <Form.Group controlId="formBasicEmail">
					    <Form.Label>Email address</Form.Label>
					    <Form.Control 
					    	type="email" 
					    	placeholder="Enter email" 
					    	value={email}
					    	onChange={(e) => setEmail(e.target.value)}
				    	/>
					    <Form.Text className="text-muted">
					      We'll never share your email with anyone else.
					    </Form.Text>
					  </Form.Group>

					  <Form.Group controlId="formBasicPassword">
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password" 
					    	value={password}
					    	onChange={(e)=> setPassword(e.target.value)}
					    />
					  </Form.Group>
					 
					  <Button variant="primary" type="submit" disabled={disable}>
					    Login
					  </Button>
					</Form>
				</Col>
			</Row>
			
		</div>
		)
}