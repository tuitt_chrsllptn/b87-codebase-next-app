import {useState, useEffect} from 'react'
import '../styles/globals.css'
//import CSS Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'
import Container from 'react-bootstrap/Container'
import AppNavBar from '../components/AppNavBar'
import {UserProvider} from '../UserContext'
import AppHelper from '../apphelper'

function MyApp({ Component, pageProps }) {
	//will contain the authenticated user details
	const [user, setUser] = useState({
		email: null,
		isAdmin: null,
		id: null
	})

	//will unset our authenticated user once the user logs out on our application
	const unsetUser = () =>{
		localStorage.clear()
		setUser({
			email: null,
			isAdmin: null,
			id: null
		})
	}

	console.log(user)
	useEffect(()=> {
		if(localStorage.getItem('token')){
			const payload = {
				headers: {Authorization: `Bearer ${AppHelper.getAccessToken()}`}
			}
			fetch(`${AppHelper.API_URL}/users/details`, payload)
			.then(res => res.json())
			.then(data => {
				setUser({
					email: data.email,
					isAdmin: data.isAdmin,
					id: data._id
				})
			})
			.catch(error => console.log(error))
		} else {
			console.log("No Authenticated user")
		}
	},[user.id])

  return (

  	<UserProvider value={{ user, setUser, unsetUser }}>
  		<AppNavBar/>
	  	<Container>
			<Component {...pageProps} />
	  	</Container>
	</UserProvider>

  	)

}

export default MyApp
