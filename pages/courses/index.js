//sol. #1
import {useState, useEffect} from 'react'
import AppHelper from '../../apphelper'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

export default function index(){
	//sol. #2
	const [courses, setCourses] = useState([])
	//sol. #3
	useEffect(()=>{
		const payload = {
			method: "GET",
			headers: {'Content-Type':'application/json'}
		}

		fetch(`${AppHelper.API_URL}/courses/`, payload)
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setCourses(data)
		})
		.catch(error => console.log(data))
	},[])


	return(
		<Row>
			{
				courses.map(course => {
					return (
					<Col md={4}>
						<Card style={{ width: '18rem' }}>
						  <Card.Body>
						    <Card.Title>{course.name}</Card.Title>
						    <Card.Subtitle className="mb-2 text-muted">{course.price}</Card.Subtitle>
						    <Card.Text>
						      {course.description}
						    </Card.Text>
						    <Card.Link href="#">Enroll</Card.Link>
						    <Card.Link href="#">Check Details</Card.Link>
						  </Card.Body>
						</Card>
					</Col>

						)
				})
			}
				
			{/*Dummy Card Containers are deleted since the data are coming from our API endpoint*/}
		</Row>
		)
}