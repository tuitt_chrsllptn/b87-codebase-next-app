//sol. no 1
import {useState, useEffect} from 'react'
import AppHelper from '../../apphelper'

import Head from 'next/head'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
export default function addcourse(){
	const [cid, setCid] = useState("")
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [price, setPrice] = useState("")
	const [startDate, setStartDate] = useState("")
	const [endDate, setEndDate] =  useState("")
	const [disable, setDisable] = useState(true)

	useEffect(()=>{
		if(cid !== "" && name !== "" && description !== "" && price !== ""){
			setDisable(false)
		} else {
			setDisable(true)
		}
	}, [cid, name, description, price])


	//sol. no 2
	const addCourse = (e) => {
		e.preventDefault()
		//sol. no. 3
		const payload = {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price:price
			})
		}

		fetch(`${AppHelper.API_URL}/courses/addCourse`,payload)
		.then(res => res.json())
		.then(data => {
			console.log(data)
		//sol. no 4
			
				if (data === true) {
					alert("Successfully added new Course")
				} else {
					alert("oops something went wrong!")
				}
			
		})
		.catch(error => console.log(error))
	}
	return(
		<div>
			<Head>
				<title>Add Course</title>
			</Head>
{/*Form groups for Course ID, Start date and End dates are comment out, we dont need this cause our API endpoint for courses structure only needs coursename, description and price*/}
			<Row className="mt-4 pt-4 justify-content-center">
				<Col md={8}>
					{/*Sol no. 5*/}
					<Form onSubmit={(e)=> addCourse(e)}>
{/*					  <Form.Group controlId="courseID">
					    <Form.Label>Course ID</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Course ID" 
					    	value={cid}
					    	onChange={(e)=> setCid(e.target.value)}
					    	/>
					  </Form.Group>
*/}
					  <Form.Group controlId="courseName">
					    <Form.Label>Course Name</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Course Name"
					    	value={name}
					    	onChange={(e)=> setName(e.target.value)}
					    />
					  </Form.Group>

					  <Form.Group controlId="courseDescription">
					    <Form.Label>Course Description</Form.Label>
					     <Form.Control 
					     	as="textarea" 
					     	rows={3} 
					     	placeholder="Enter Course Description"
					     	value={description}
					     	onChange={(e)=> setDescription(e.target.value)}
					     />
					  </Form.Group>

					  <Form.Group controlId="coursePrice">
					    <Form.Label>Course Price</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Course Price" 
					    	value={price}
					     	onChange={(e)=> setPrice(e.target.value)}
					    />
					  </Form.Group>

					 {/* <Form.Group controlId="courseStartDate">
					    <Form.Label>Start Date</Form.Label>
					    <Form.Control 
					    	type="text" 
					    	placeholder="Enter Start Date" 
					    	value={startDate}
					     	onChange={(e)=> setStartDate(e.target.value)}
					    />
					  </Form.Group>

					  <Form.Group controlId="courseEndDate">
					    <Form.Label>End Date</Form.Label>
					    <Form.Control 
						    type="text" 
						    placeholder="Enter End Date" 
						    value={endDate}
					     	onChange={(e)=> setEndDate(e.target.value)}
						   />
					  </Form.Group>*/}
					
					  <Button variant="primary" type="submit">
					    Add Course
					  </Button>
					</Form>
				</Col>
			</Row>

		</div>

		)
}


/*
	Activity instructions:

	1. Create the states for the ff values:
		course id, course name, course description, course price, start date and end date
	2. Bind the states to their necessary input fields
	3. Use effect hook to check the values for the courses and create the ff condition:
		a. if course id, name, description and price is not empty, enable the Add course button
		b. if letter a is not met, disable the button

	9:25pm



*/