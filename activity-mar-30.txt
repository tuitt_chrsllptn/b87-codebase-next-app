--- Fetching courses for our `pages/courses/index.js` ---
Part 1
Instruction:

1. On our file pages/courses/index.js, import the apphelper needed for the api_url to fetch courses, state and effect hooks to contain and manipulate our fetched courses
2. Create a state called 'courses' that shall contain our course when fetched on our API endpoint
3. Use effect hook that will fetch the courses from our API_URL `/courses/`, then set the fetched data on course setter.
4. Map the courses using the Card container, display the course name, description, price and an enroll button

--- 30 mins ---


--- Creating course using our form on `pages/courses/addcourse.js ---
Part 2

Instruction:
1. On our file pages/courses/addcourse.js, import the apphelper needd for the api_url to fetch add course
2 Create a function called addCourse that receives an 'e' parameter, to be used for 'e.preventDefault()'
3. Inside the function addCourse, create a functionality that inserts a new course on our API endpoint called `/courses/addCourse`, with the course name, price and description
4. Create an alert that will tell "Successfully added a new course" if the course is added, else, alert "Oops something went wrong!"
5. Bind the function addCourse on the Form onSubmit event

--- 30 mins ---