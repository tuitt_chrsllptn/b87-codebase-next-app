//the goal of this app helper file is to shorten written code in various parts of the project

module.exports = {
	API_URL: process.env.NEXT_PUBLIC_API_URL,
	getAccessToken: () => localStorage.getItem('token')
}