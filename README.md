## Next JS App Creation
To create a new Next JS App run command below:
```bash
npx create-next-app appName
```

After creating, you can serve the app by running command below:
```bash
npm run dev
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Pages and Navigation
App entry point is under `pages/_app.js`. You can import packages here that you want to be implemented globally

NextJS has a file system routing, it means, the files created under `pages/` directory are created also with a URL endpoint.

ex. `pages/login.js` has URL endpoint on our app `http://localhost:3000/login`

## Setting up our .env.local file

To set up our environment variables for our app, first create a file called `.env.local` on our app root directory. This will contain the apps environment variables like the API URL endpoint for fetching data for our project.

Then inside the `.env.local` create a variable starting with words <b>NEXT_PUBLIC</b> to expose our env var on our app, then followed by the name of our env var.
ex. <b>NEXT_PUBLIC_API_URL=value</b>

On this part, we will add our API URL on our env file, please add the ff on our `.env.local`

<b>NEXT_PUBLIC_API_URL=http://localhost:4000/api</b>

## Creating app helper file
